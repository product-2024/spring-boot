package com.example.demo.repository.dao;

import com.example.demo.entity.Message;
import java.util.List;
import org.seasar.doma.BatchInsert;
import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;
import org.seasar.doma.jdbc.BatchResult;
import org.seasar.doma.jdbc.Result;
import org.springframework.transaction.annotation.Transactional;

@Dao
@ConfigAutowireable
@Transactional
public interface MessageDao {
    @Select
    List<Message> selectAll();

    @Insert
    Result<Message> insert(Message message);

    @BatchInsert
    BatchResult<Message> bulkinsert(List<Message> messages);
}
