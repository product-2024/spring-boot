package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class SystemProperty {

    @Autowired
    Environment environment;

    @Bean
    public String getProperty() {
        return environment.getProperty("SERVER_NAME");
    }

}
