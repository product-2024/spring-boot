package com.example.demo.config;

import com.example.demo.platform.DomaJdbcLogger;
import com.zaxxer.hikari.HikariConfig;
import javax.sql.DataSource;
import org.seasar.doma.jdbc.Config;
import org.seasar.doma.jdbc.JdbcLogger;
import org.seasar.doma.jdbc.dialect.Dialect;
import org.seasar.doma.jdbc.dialect.OracleDialect;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DomaConfig extends HikariConfig implements Config {

    public DataSource dataSource() {
        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:oracle:thin:@192.168.33.10:1521/OPEDB");
        dataSourceBuilder.username("user01");
        dataSourceBuilder.password("password");
        return dataSourceBuilder.build();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public Dialect dialect() {
        return new OracleDialect();
    }

    @Override
    public Dialect getDialect() {
        return dialect();
    }

    @Override
    public DataSource getDataSource() {
        return new TransactionAwareDataSourceProxy(dataSource());
    }

    @Bean
    public JdbcLogger getDomaJdbcLogger() {
        return new DomaJdbcLogger();
    }

    @Override
    public JdbcLogger getJdbcLogger() {
        return getDomaJdbcLogger();
    }
}
