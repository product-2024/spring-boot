package com.example.demo.config;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.ObjectUtils;

@Configuration
@PropertySource(value = {"classpath:clock.txt"})
public class ClockConfig {

    private static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private static final String ZONE_ID = "Asia/Tokyo";

    @Value("${startup.reference.time}")
    private String startupReferenceTime;

    @Bean
    public Clock custumClock() {
        if (ObjectUtils.isEmpty(startupReferenceTime)) {
            System.out.println("Using default clock");
            return Clock.system(ZoneId.of(ZONE_ID));
        } else {
            System.out.println(String.format("Using custom clock: %s", startupReferenceTime));
            LocalDateTime targetDateTime = LocalDateTime.parse(startupReferenceTime,
                    DateTimeFormatter.ofPattern(DATE_FORMAT));
            LocalDateTime currentDateTime = LocalDateTime.now();
            Duration duration = Duration.between(currentDateTime, targetDateTime);
            Clock clock = Clock.system(ZoneId.of(ZONE_ID));
            return Clock.offset(clock, duration);
        }
    }

    // @PropertySourceはワイルドカード指定ができないため一つのファイルしか指定できないのに対して、
    // PropertySourcesPlaceholderConfigurerは複数のファイルを指定することができる
    // @Bean
    // public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    // return new PropertySourcesPlaceholderConfigurer();
    // }

}
