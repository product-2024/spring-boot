package com.example.demo.config;

import com.example.demo.platform.CustomFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

@Configuration
public class FilterConfig {
    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Bean
    public FilterRegistrationBean<CustomFilter> filterConfg(CustomFilter customfilter) {
        FilterRegistrationBean<CustomFilter> registrationBean =
                new FilterRegistrationBean<CustomFilter>(customfilter);
        // フィルターの適用URLを設定する
        registrationBean.addUrlPatterns("/*");
        // フィルターの実行順序を設定する
        registrationBean.setOrder(1);
        return registrationBean;
    }
}
