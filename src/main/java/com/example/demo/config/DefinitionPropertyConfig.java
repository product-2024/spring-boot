package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:definition/definition4eee.properties", encoding = "UTF-8")
@PropertySource(value = "classpath:definition/definition4ppp.properties", encoding = "UTF-8")
@PropertySource(value = "classpath:definition/definition4ttt.properties", encoding = "UTF-8")
public class DefinitionPropertyConfig {
}
