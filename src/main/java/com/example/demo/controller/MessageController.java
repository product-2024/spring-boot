package com.example.demo.controller;

import com.example.demo.config.SystemProperty;
import com.example.demo.entity.Message;
import com.example.demo.platform.LogStartEnd;
import com.example.demo.repository.dao.MessageDao;
import com.google.common.collect.Lists;
import java.util.List;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MessageController {
    private final MessageDao messageDao;
    private final SystemProperty property;

    public MessageController(MessageDao messageDao, SystemProperty property) {
        this.messageDao = messageDao;
        this.property = property;
    }

    @LogStartEnd(start = "TEST001", end = "TEST002")
    @RequestMapping("/list")
    List<Message> list() {
        // viewFilters();
        System.out.println(property.getProperty());
        List<Message> list = messageDao.selectAll();
        return list;
    }

    @RequestMapping("/message")
    Message add(@RequestParam(value = "text") String text) {
        Message message = Message.builder().text(text).build();
        messageDao.insert(message);
        return message;
    }

    @RequestMapping("/bulkInsert")
    public void bulkInsert() {
        List<Message> test = Lists.newArrayList();
        test.add(Message.builder().text("これはサンプルです1").build());
        test.add(Message.builder().text("これはサンプルです2").build());
        test.add(Message.builder().text("これはサンプルです3").build());
        test.add(Message.builder().text("これはサンプルです4").build());
        test.add(Message.builder().text("これはサンプルです5").build());
        test.add(Message.builder().text("これはサンプルです6").build());
        test.add(Message.builder().text("これはサンプルです7").build());
        test.add(Message.builder().text("これはサンプルです8").build());
        test.add(Message.builder().text("これはサンプルです9").build());
        test.add(Message.builder().text("これはサンプルです10").build());
        messageDao.bulkinsert(test);
    }

    @Autowired
    private ServletContext servletContext;

    public void viewFilters() {
        java.util.Set<String> filterNames = servletContext.getFilterRegistrations().keySet();
        for (String filterName : filterNames) {
            FilterRegistration filterRegistration =
                    servletContext.getFilterRegistration(filterName);
            System.out.println("Filter Name: " + filterName);
            System.out.println("Filter Class: " + filterRegistration.getClassName());
            System.out.println("URL Patterns: " + filterRegistration.getUrlPatternMappings());
        }
    }

}
