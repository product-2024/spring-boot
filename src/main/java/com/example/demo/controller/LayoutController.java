package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * HelloControllerクラスです。
 */
@Controller
public class LayoutController {
    @GetMapping("/sample")
    public String method() {
        return "content";
    }
}
