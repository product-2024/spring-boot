package com.example.demo.controller;

import com.example.demo.platform.CustomLocalDateTime;
import com.example.demo.platform.PropertyProvider;
import com.example.demo.platform.UserContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloRestController {

    @Autowired
    UserContext usercontext;

    @Autowired
    @Qualifier("reloadableResourceBundleMessageSource")
    MessageSource messageSource;

    @Data
    @Builder
    public static class Response {
        private LocalDateTime localDateTime;
        private LocalDate localDate;
        private LocalTime localTime;
    }

    @GetMapping("/hello")
    public Map<String, String> hello() {
        System.out.println("HelloRestController.javaに到達している");
        return Map.of("msg", usercontext.getRoleId() + " " + usercontext.getUserId());
    }

    @GetMapping(path = "/api/datetime")
    public Response getCurrentDatetime() {
        LocalDateTime date = CustomLocalDateTime.customLocalDateTime();
        return Response.builder().localDateTime(date).localDate(date.toLocalDate())
                .localTime(date.toLocalTime()).build();
    }

    @Autowired
    PropertyProvider property;

    @GetMapping("/property")
    public Integer getValue() {
        return property.getIntegerOf("xxx.test");
    }

    @GetMapping("/message")
    public String message() {
        return messageSource.getMessage("E22345", null, null);
    }
}
