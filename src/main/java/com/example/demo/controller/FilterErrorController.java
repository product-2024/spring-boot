package com.example.demo.controller;

import javax.naming.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class FilterErrorController {

    // フィルターで例外発生時に呼び出されるメソッド。
    @ExceptionHandler(AuthenticationException.class)
    public String handleError() {
        System.out.println("handleError: ここに来るはず。");
        // request.setAttribute("error", exception);
        // request.setAttribute("message", "Exceptionが発生しました");
        // request.setAttribute("status", HttpStatus.UNAUTHORIZED);
        return "error";
    }
}
