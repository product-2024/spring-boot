package com.example.demo.platform;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class CustomFilter extends OncePerRequestFilter {

    @Autowired
    UserContext usercontext;

    @Value("${aaa.test}")
    private String[] date;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest req,
            @NonNull HttpServletResponse res, @NonNull FilterChain chain)
            throws ServletException, IOException {
        try {
            HttpServletRequest httpRequest = (HttpServletRequest) req;
            Map<String, String> headers = Collections.list(httpRequest.getHeaderNames()).stream()
                    .collect(Collectors.toMap(h -> h, httpRequest::getHeader));

            if (headers.get("user_id") != null && headers.get("role_id") != null) {
                usercontext.setUserId(headers.get("user_id"));
                usercontext.setRoleId(headers.get("role_id"));
                System.out.println("ここまで");
                System.out.println(date[0] + " " + date[1]);
            } else {
                throw new AuthenticationException("HTTPリクエストヘッダ設定不備の可能性があります");
            }
            chain.doFilter(req, res);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // FilterConfigでフィルター適用対象としたURLのうち適用除外とするURLリストを設定
    private Set<String> skipUrls = new HashSet<>(Arrays.asList("/api/*", "/message"));
    private AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return skipUrls.stream().anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
    }
}
