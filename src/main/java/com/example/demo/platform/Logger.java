package com.example.demo.platform;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Locale;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

public class Logger {
    private static final String UNDEFINED_MESSAGE_FORMAT = "UNDEFINED-MESSAGE id:{0} arg:{1}";

    private static ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

    private String message;

    public Logger(String name) {
        this.message = name;
    }

    String getMassage() {
        return this.message;
    }

    @SuppressWarnings("unused")
    private String getMessage(String id, Object... args) {
        String message;
        try {
            message = messageSource.getMessage(id, args, Locale.getDefault());
        } catch (NoSuchMessageException e) { // (10)
            message = MessageFormat.format(UNDEFINED_MESSAGE_FORMAT, id, Arrays.toString(args));
        }
        return message;
    }
}
