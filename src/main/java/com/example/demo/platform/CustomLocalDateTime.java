package com.example.demo.platform;

import java.time.Clock;
import java.time.LocalDateTime;

public class CustomLocalDateTime {

    // @Autowiredはstatic変数に設定できない
    private static Clock clock;

    public static void setClockConfig(Clock clock) {
        CustomLocalDateTime.clock = clock;
    }

    public static LocalDateTime customLocalDateTime() {
        return LocalDateTime.now(clock);
    }
}
