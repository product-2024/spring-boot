package com.example.demo.platform;

import java.util.LinkedList;
import org.seasar.doma.jdbc.Sql;
import org.seasar.doma.jdbc.UtilLoggingJdbcLogger;

public class DomaJdbcLogger extends UtilLoggingJdbcLogger {

    private final ThreadLocal<LinkedList<Sql<?>>> sqlLogList = new ThreadLocal<>();

    @Override
    public void logSql(String callerClassName, String callerMethodName, Sql<?> sql) {
        add(sql);
        super.logSql(callerClassName, callerMethodName, sql);
    }

    public LinkedList<Sql<?>> getSqlList() {
        return isEmpty() ? null : getSqlLogList();
    }

    public void remove() {
        this.sqlLogList.remove();
    }

    private void add(Sql<?> sql) {
        LinkedList<Sql<?>> list = getSqlLogList();
        list.add(sql);
    }

    private LinkedList<Sql<?>> getSqlLogList() {
        LinkedList<Sql<?>> list = (LinkedList<Sql<?>>) sqlLogList.get();
        if (list == null) {
            list = new LinkedList<>();
            sqlLogList.set(list);
        }
        return list;
    }

    private boolean isEmpty() {
        return getSqlLogList().size() == 0;
    }
}
