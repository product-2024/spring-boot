package com.example.demo.platform;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogStartEndAspect {

    @Around("@annotation(com.example.demo.platform.LogStartEnd)")
    public Object logMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        LogStartEnd annotation = signature.getMethod().getAnnotation(LogStartEnd.class);

        String startMessageId = annotation.start();
        String endMessageId = annotation.end();

        System.out.println(startMessageId);

        Object result = joinPoint.proceed();

        System.out.println(endMessageId);

        return result;
    }
}
