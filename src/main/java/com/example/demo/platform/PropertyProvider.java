package com.example.demo.platform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class PropertyProvider {

    @Autowired
    private Environment env;

    public String getStringOf(String key) {
        return env.getRequiredProperty(key, String.class);
    }

    public Integer getIntegerOf(String key) {
        return env.getRequiredProperty(key, Integer.class);
    }

    public Double getDoubleOf(String key) {
        return env.getRequiredProperty(key, Double.class);
    }

    public Long getLongOf(String key) {
        return env.getRequiredProperty(key, Long.class);
    }
}
