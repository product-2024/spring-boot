package com.example.demo.platform;

import java.time.Clock;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupShutdownEventListener {

    private static Long TIME = 3000L;

    @Autowired
    private Clock custumClock;

    @EventListener(ApplicationReadyEvent.class)
    void onStartup() throws InterruptedException {
        CustomLocalDateTime.setClockConfig(custumClock);
        Thread.sleep(TIME);
        String date = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS")
                .format(CustomLocalDateTime.customLocalDateTime());
        System.out.println("SIM TIME : [ " + date + " ] アプリケーションが正常に起動しました。");
    }

    @EventListener(ContextClosedEvent.class)
    void onShutdown() {
        System.out.println("アプリケーションが正常に停止しました。");
    }
}
