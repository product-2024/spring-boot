package com.example.demo.platform;

import java.util.LinkedList;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.seasar.doma.jdbc.JdbcException;
import org.seasar.doma.jdbc.Sql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Aspect
@Component
public class AuditLogAspect {

    // 空白文字2回以上 または 改行
    private static final String REGEX_PATTERN = "\\s{2,}+|" + System.lineSeparator();

    @Autowired
    DomaJdbcLogger domaJdbcLogger;

    @After("execution(* com.example.demo.repository..*Impl.*(..))")
    public void auditLog(JoinPoint jp) {

        try {
            LinkedList<Sql<?>> sqlList = domaJdbcLogger.getSqlList();
            if (CollectionUtils.isEmpty(sqlList)) {
                System.out.println("-----------------------------------");
                // System.out.println(domaJdbcLogger.getSqlExecutionSkipCause());
                System.out.println("-----------------------------------");
                return;
            }
            for (Object sql : sqlList.toArray()) {
                System.out.println("-----------------------------------");
                // 実行SQL文
                System.out.println("FormattedSql: " + ((Sql<?>) sql).getFormattedSql()
                        .replaceAll(REGEX_PATTERN, StringUtils.SPACE));
                // 実行SQL種別
                System.out.println("SqlKind: " + ((Sql<?>) sql).getKind());
                // 実行SQLにおけるパラメータ
                ((Sql<?>) sql).getParameters().stream()
                        .forEach(x -> System.out.println(x.getValue()));
                System.out.println("-----------------------------------");
            }
        } finally {
            domaJdbcLogger.remove();
        }
    }

    @AfterThrowing(pointcut = "execution(* com.example.demo.repository..*Impl.*(..))",
            throwing = "ex")
    public void handleException(JoinPoint joinPoint, JdbcException ex) {
        System.out.println("-----------------------------------");
        System.out.println(ex.getCause());
        System.out.println(ex.getStackTrace());
        System.out.println("-----------------------------------");
    }
}

