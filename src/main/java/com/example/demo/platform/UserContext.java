package com.example.demo.platform;

import java.io.Serializable;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
@Data
public class UserContext implements Serializable {
    private static final long serialVersionUID = -533538126274200415L;
    private String userId;
    private String roleId;
}
