package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * DemoApplicationクラスです。
 */
@SpringBootApplication
public class DemoApplication extends SpringBootServletInitializer {

    /**
     * エントリーポイントです。
     *
     * @param args 引数
     */
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DemoApplication.class);
        application.run(args);
    }
}
