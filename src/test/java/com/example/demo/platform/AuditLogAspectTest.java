package com.example.demo.platform;

import static org.mockito.Mockito.when;
import java.util.Collections;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.seasar.doma.jdbc.PreparedSql;
import org.seasar.doma.jdbc.Sql;
import org.seasar.doma.jdbc.SqlKind;
import org.seasar.doma.jdbc.SqlLogType;

@ExtendWith(MockitoExtension.class)
public class AuditLogAspectTest {
    @Mock
    private JoinPoint joinPoint;

    @Mock
    private DomaJdbcLogger domaJdbcLogger;

    @InjectMocks
    private AuditLogAspect aspect;

    @Test
    void testAuditLog() {
        when(domaJdbcLogger.getSqlList()).thenReturn(new LinkedList<Sql<?>>());
        aspect.auditLog(joinPoint);
    }

    @Test
    void testAuditLog2() {
        LinkedList<Sql<?>> sqlList = new LinkedList<Sql<?>>();
        PreparedSql select_sql = new PreparedSql(SqlKind.SELECT, "aaa", "bbb", "ccc",
                Collections.emptyList(), SqlLogType.NONE);
        PreparedSql update_sql = new PreparedSql(SqlKind.UPDATE, "ddd", "eee", "fff",
                Collections.emptyList(), SqlLogType.NONE);
        sqlList.add(select_sql);
        sqlList.add(update_sql);

        when(domaJdbcLogger.getSqlList()).thenReturn(sqlList);
        aspect.auditLog(joinPoint);
    }
}
