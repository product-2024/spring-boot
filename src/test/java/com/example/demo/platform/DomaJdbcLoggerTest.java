package com.example.demo.platform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.seasar.doma.jdbc.PreparedSql;
import org.seasar.doma.jdbc.SqlKind;
import org.seasar.doma.jdbc.SqlLogType;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

@ExtendWith(OutputCaptureExtension.class)
public class DomaJdbcLoggerTest {

    @Test
    void testGetSqlList(CapturedOutput output) {
        DomaJdbcLogger logger = new DomaJdbcLogger();

        PreparedSql select_sql = new PreparedSql(SqlKind.SELECT, "aaa", "bbb", "ccc",
                Collections.emptyList(), SqlLogType.NONE);
        logger.logSql("ddd", "eee", select_sql);

        PreparedSql update_sql = new PreparedSql(SqlKind.UPDATE, "ddd", "eee", "fff",
                Collections.emptyList(), SqlLogType.NONE);
        logger.logSql("fff", "ggg", update_sql);

        assertEquals(2, logger.getSqlList().size());
        assertTrue(output.getAll().contains("[DOMA2076] SQL LOG : PATH=[fff]"));
    }

    @Test
    void testGetSqlList_empty() {
        DomaJdbcLogger logger = new DomaJdbcLogger();
        assertEquals(null, logger.getSqlList());
    }

    @Test
    void testLogSql() {
        DomaJdbcLogger logger = new DomaJdbcLogger();
        PreparedSql sql = new PreparedSql(SqlKind.SELECT, "aaa", "bbb", "ccc",
                Collections.emptyList(), SqlLogType.NONE);

        logger.logSql("ddd", "eee", sql);
        assertEquals(1, logger.getSqlList().size());
    }
}
