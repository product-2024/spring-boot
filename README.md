`.vscode/settings.json`

```json
{
    "java.format.settings.profile": "GoogleStyle",
    "java.format.settings.url": "config/formatter/eclipse-java-google-style.xml",
    "editor.formatOnSave": true,
    "[java]": {
        "editor.insertSpaces": true,
        "editor.tabSize": 4,
        "editor.detectIndentation": true
    },
    "java.configuration.updateBuildConfiguration": "automatic",
    "java.compile.nullAnalysis.mode": "automatic",
    "java.completion.importOrder": [
        "java",
        "javax"
    ]
}
```

## 環境変数設定

```
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-DSERVER_NAME=test"
```
