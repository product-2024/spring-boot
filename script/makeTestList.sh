PWD=$(cd $(dirname $0); pwd)
cd $PWD/../
find src/test -name '*.java' -type f | xargs grep -H 'void' | sed 's/ {//g' | sed 's/src\/test\/java\///g' | sed 's/:  void//' | cut -f 1 -d ' '
cd - &>/dev/null
