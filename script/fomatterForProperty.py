import os
import glob
from enum import Enum

# スクリプトファイルのディレクトリパス取得
current_dir = os.path.dirname(os.path.abspath(__file__))
# 一階層上のディレクトリパス取得
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
# メッセージフォルダ
target_message_dir = "src/main/resources/message"
# 検索対象ファイル
target_message_file_pattern = "message4*.properties"
# メッセージフォーマット
MESSAGE_FORMATTER = 'XXX{}=XXX,{},{}'
# 出力ファイル
output_file = "tmp.properties"

class LOG_LEVEL(Enum):
  INFO     = "1"
  WARNING  = "2"
  ERROR    = "3"
  DEBUG    = "4"

class FUNC_DEF(Enum):
  eee = "eee"
  ppp = "ppp"
  ttt = "ttt"

def get_func_comment(file_name):
  target = file_name[8:11]
  if target == FUNC_DEF.eee.name:
    return FUNC_DEF.eee.value
  elif target == FUNC_DEF.ppp.name:
    return FUNC_DEF.ppp.value
  elif target == FUNC_DEF.ttt.name:
    return FUNC_DEF.ttt.value

def read_property(file):
  property_file_list = []
  property_file_list.append('# ' + get_func_comment(os.path.basename(file)))
  with open(file, "r", encoding = "UTF-8") as f:
    for line in f:
      check = False
      # コメント行及び空白行は対象外
      if '#' not in line and (not line.isspace()):
        message = line.rstrip().split("=")
        print(message)
        foo = message[0][1]
        error_type = ""
        if foo == LOG_LEVEL.INFO.value:
          error_type = LOG_LEVEL.INFO.name
          check = True
        elif foo == LOG_LEVEL.WARNING.value:
          error_type = LOG_LEVEL.WARNING.name
          check = True
        elif foo == LOG_LEVEL.ERROR.value:
          error_type = LOG_LEVEL.ERROR.name
          check = True
        elif foo == LOG_LEVEL.DEBUG.value:
          print("--> DBUGログのため、変換対象外としてスキップする。")
          print("")
          continue

      if check == True:
        formated_line = MESSAGE_FORMATTER.format(message[0],error_type,message[1])
        print("--> " + formated_line)
        print("")
        property_file_list.append(formated_line)
  return sorted(property_file_list)

def get_properties():
  # 変換対象ファイル一覧
  return sorted(glob.glob(os.path.join(parent_dir, target_message_dir, target_message_file_pattern)))

if __name__ == '__main__':
  target_file = os.path.join(parent_dir, target_message_dir, output_file)
  if(os.path.isfile(target_file)):
    os.remove(target_file)

  for file in get_properties():
    with open(target_file, mode='a', encoding='utf-8', newline='\n') as f:
      for line in read_property(file):
        f.write(line + '\n')

  print('{}の生成が完了しました。'.format(output_file))